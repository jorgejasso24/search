# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Vehicle', type: :request do
  let(:auction1_atts) { {} }
  let(:auction2_atts) { {} }
  let(:auction3_atts) { {} }
  let!(:auction1) { FactoryBot.create(:company, auction1_atts) }
  let!(:auction2) { FactoryBot.create(:company, auction2_atts) }
  let!(:auction3) { FactoryBot.create(:company, auction3_atts) }

  subject do
    post '/graphql', params: { query: query }
    JSON.parse(response.body)
  end

  describe '#auction' do
    let(:query) do
      <<~GQL
        { auction }
      GQL
    end

    context 'vehicle auctions' do
      let(:auction1_atts) { { name: 'America Auto Auction - Boston' } }
      let(:auction2_atts) { { name: 'DAA Northwest' } }
      let(:auction3_atts) { { name: 'Long Beach Auto Auction' } }

      it do
        expect(subject.dig('data', 'auction')).to contain_exactly(
          auction1.name, auction2.name, auction3.name
        )
      end
    end
  end
end
