# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Vehicle', type: :request do
  let(:consignor1_atts) { {} }
  let(:consignor2_atts) { {} }
  let(:consignor3_atts) { {} }
  let!(:consignor1) { FactoryBot.create(:consignor, consignor1_atts) }
  let!(:consignor2) { FactoryBot.create(:consignor, consignor2_atts) }
  let!(:consignor3) { FactoryBot.create(:consignor, consignor3_atts) }

  subject do
    post '/graphql', params: { query: query }
    JSON.parse(response.body)
  end

  describe '#consignor' do
    let(:query) do
      <<~GQL
        { consignor }
      GQL
    end

    context 'vehicle consignors' do
      let(:consignor1_atts) { { name: 'Anderson Ford' } }
      let(:consignor2_atts) { { name: 'Auto World U.S.A' } }
      let(:consignor3_atts) { { name: 'Holiday Financial Services' } }

      it do
        expect(subject.dig('data', 'consignor')).to contain_exactly(
          consignor1.name, consignor2.name, consignor3.name
        )
      end
    end
  end
end
