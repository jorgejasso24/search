# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Vehicle', type: :request do
  let(:vehicle1_atts) { {} }
  let(:vehicle2_atts) { {} }
  let(:vehicle3_atts) { {} }
  let!(:vehicle1) { FactoryBot.create(:vehicle, vehicle1_atts) }
  let!(:vehicle2) { FactoryBot.create(:vehicle, vehicle2_atts) }
  let!(:vehicle3) { FactoryBot.create(:vehicle, vehicle3_atts) }
  let(:args_string) {}

  subject do
    post '/graphql', params: { query: query }
    JSON.parse(response.body)
  end

  describe '#find_vehicle_by_vin' do
    let(:query) do
      <<~GQL
        {
          findVehicleByVin(#{args_string}) {
            vin
            year
            grade
            odometer
            make
            model
            trim
            bodyStyle
            fuelType
            hasConditionReport
            has360
            consignor {
              name
            }
            auction:company {
              name
            }
          }
        }
      GQL
    end

    it do
      expect(subject.dig('errors', 0, 'message')).to eq(
        "Field 'findVehicleByVin' is missing required arguments: vin"
      )
    end

    context 'when VIN does not exist' do
      let(:args_string) { 'vin: X1Y65450UXO099070' }

      it { expect(subject.dig('errors', 0, 'message')).to eq('El VIN no existe') }
    end

    context 'when VIN exists' do
      let(:vehicle1_atts) { { vin: 'G2M4I4J1XQY507162' } }
      let(:args_string) { 'vin: G2M4I4J1XQY507162' }

      it do
        expect(subject.dig('data', 'findVehicleByVin')).to contain_exactly(
          ['vin', vehicle1.vin],
          ['year', vehicle1.year],
          ['grade', vehicle1.grade.to_i],
          ['odometer', vehicle1.odometer],
          ['make', vehicle1.make],
          ['model', vehicle1.model],
          ['trim', vehicle1.trim],
          ['bodyStyle', vehicle1.body_style],
          ['fuelType', vehicle1.fuel_type],
          ['hasConditionReport', vehicle1.has_condition_report],
          ['has360', vehicle1.has_360],
          ['consignor', { 'name' => vehicle1.consignor.name }],
          ['auction', { 'name' => vehicle1.company.name }]
        )
      end
    end
  end

  describe '#years' do
    let(:query) do
      <<~GQL
        { years }
      GQL
    end

    context 'vehicle years' do
      let(:vehicle1_atts) { { year: 2019 } }
      let(:vehicle2_atts) { { year: 2018 } }
      let(:vehicle3_atts) { { year: 2017 } }

      it do
        expect(subject.dig('data', 'years')).to contain_exactly(
          vehicle1.year, vehicle2.year, vehicle3.year
        )
      end
    end
  end

  describe '#grade' do
    let(:query) do
      <<~GQL
        { grade }
      GQL
    end

    context 'vehicle grade' do
      let(:vehicle1_atts) { { grade: '1.0' } }
      let(:vehicle2_atts) { { grade: '2.0' } }
      let(:vehicle3_atts) { { grade: '3.0' } }

      it do
        expect(subject.dig('data', 'grade')).to contain_exactly(
          vehicle1.grade, vehicle2.grade, vehicle3.grade
        )
      end
    end
  end

  describe '#odometer' do
    let(:query) do
      <<~GQL
        { odometer }
      GQL
    end

    context 'vehicle odometer' do
      let(:vehicle1_atts) { { odometer: 10_000 } }
      let(:vehicle2_atts) { { odometer: 20_000 } }
      let(:vehicle3_atts) { { odometer: 30_000 } }

      it do
        expect(subject.dig('data', 'odometer')).to contain_exactly(
          vehicle1.odometer, vehicle2.odometer, vehicle3.odometer
        )
      end
    end
  end

  describe '#makes' do
    let(:query) do
      <<~GQL
        { makes }
      GQL
    end

    context 'vehicle makes' do
      let(:vehicle1_atts) { { make: 'Toyota' } }
      let(:vehicle2_atts) { { make: 'Honda' } }
      let(:vehicle3_atts) { { make: 'Ford' } }

      it do
        expect(subject.dig('data', 'makes')).to contain_exactly(
          vehicle1.make, vehicle2.make, vehicle3.make
        )
      end
    end
  end

  describe '#models' do
    let(:query) do
      <<~GQL
        { models(#{args_string}) }
      GQL
    end

    it do
      expect(subject.dig('errors', 0, 'message')).to eq(
        "Field 'models' is missing required arguments: make"
      )
    end

    context 'when make does not exist' do
      let(:args_string) { 'make: [non-existent-make]' }
      let(:vehicle1_atts) { { make: 'Toyota' } }
      let(:vehicle2_atts) { { make: 'Honda' } }
      let(:vehicle3_atts) { { make: 'Toyota' } }

      it { expect(subject.dig('data', 'models')).to eq(nil) }
    end

    context 'when make exists' do
      let(:args_string) { 'make: [Toyota]' }
      let(:vehicle1_atts) { { make: 'Toyota', model: 'Supra' } }
      let(:vehicle2_atts) { { make: 'Honda', model: 'Pilot' } }
      let(:vehicle3_atts) { { make: 'Toyota', model: 'Camry' } }

      it do
        expect(subject.dig('data', 'models')).to contain_exactly(
          vehicle1.model, vehicle3.model
        )
      end
    end
  end

  describe '#trim' do
    let(:query) do
      <<~GQL
        { trim(#{args_string}) }
      GQL
    end

    context 'when there are not parameters' do
      it do
        expect(subject.dig('errors', 0, 'message')).to eq(
          "Field 'trim' is missing required arguments: make, model"
        )
      end

      context 'and the missing parameter is make' do
        let(:args_string) { 'model: [Supra]' }

        it do
          expect(subject.dig('errors', 0, 'message')).to eq(
            "Field 'trim' is missing required arguments: make"
          )
        end
      end

      context 'and the missing parameter is model' do
        let(:args_string) { 'make: [Toyota]' }

        it do
          expect(subject.dig('errors', 0, 'message')).to eq(
            "Field 'trim' is missing required arguments: model"
          )
        end
      end
    end

    context 'when parameters do not exist' do
      let(:args_string) { 'make: [non-existent-make], model: [Supra]' }
      let(:vehicle1_atts) { { make: 'Toyota' } }
      let(:vehicle2_atts) { { make: 'Honda' } }
      let(:vehicle3_atts) { { make: 'Toyota' } }

      it { expect(subject.dig('data', 'trim')).to eq(nil) }
    end

    context 'when parameters exist' do
      let(:args_string) { 'make: [Toyota], model: [Supra]' }
      let(:vehicle1_atts) { { make: 'Toyota', model: 'Supra', trim: 'SE' } }
      let(:vehicle2_atts) { { make: 'Honda', model: 'Pilot', trim: 'EX' } }
      let(:vehicle3_atts) { { make: 'Toyota', model: 'Camry', trim: 'Hybrid XLE' } }

      it { expect(subject.dig('data', 'trim')).to contain_exactly(vehicle1.trim) }
    end
  end

  describe '#body' do
    let(:query) do
      <<~GQL
        { body }
      GQL
    end

    context 'vehicle body_style' do
      let(:vehicle1_atts) { { body_style: 'Cargo Van' } }
      let(:vehicle2_atts) { { body_style: 'Convertible' } }
      let(:vehicle3_atts) { { body_style: 'Passenger Van' } }

      it do
        expect(subject.dig('data', 'body')).to contain_exactly(
          vehicle1.body_style, vehicle2.body_style, vehicle3.body_style
        )
      end
    end
  end

  describe '#fuel' do
    let(:query) do
      <<~GQL
        { fuel }
      GQL
    end

    context 'vehicle fuel_type' do
      let(:vehicle1_atts) { { fuel_type: 'E-85/Gasoline' } }
      let(:vehicle2_atts) { { fuel_type: 'Gasoline Hybrid' } }
      let(:vehicle3_atts) { { fuel_type: 'Gasoline Hybrid' } }

      it do
        expect(subject.dig('data', 'fuel')).to contain_exactly(
          vehicle1.fuel_type, vehicle2.fuel_type
        )
      end
    end
  end

  describe '#search' do
    let(:query) do
      <<~GQL
        {
          search(#{args_string}) {
            vin
            year
            grade
            odometer
            make
            model
            trim
            bodyStyle
            fuelType
            hasConditionReport
            has360
            consignor {
              name
            }
            auction: company {
              name
            }
          }
        }
      GQL
    end
    let(:vehicle1_expected) do
      {
        'vin' => vehicle1.vin,
        'year' => vehicle1.year,
        'grade' => vehicle1.grade.to_i,
        'odometer' => vehicle1.odometer,
        'make' => vehicle1.make,
        'model' => vehicle1.model,
        'trim' => vehicle1.trim,
        'bodyStyle' => vehicle1.body_style,
        'fuelType' => vehicle1.fuel_type,
        'hasConditionReport' => vehicle1.has_condition_report,
        'has360' => vehicle1.has_360,
        'consignor' => { 'name' => vehicle1.consignor.name },
        'auction' => { 'name' => vehicle1.company.name }
      }
    end
    let(:vehicle2_expected) do
      {
        'vin' => vehicle2.vin,
        'year' => vehicle2.year,
        'grade' => vehicle2.grade.to_i,
        'odometer' => vehicle2.odometer,
        'make' => vehicle2.make,
        'model' => vehicle2.model,
        'trim' => vehicle2.trim,
        'bodyStyle' => vehicle2.body_style,
        'fuelType' => vehicle2.fuel_type,
        'hasConditionReport' => vehicle2.has_condition_report,
        'has360' => vehicle2.has_360,
        'consignor' => { 'name' => vehicle2.consignor.name },
        'auction' => { 'name' => vehicle2.company.name }
      }
    end
    let(:vehicle3_expected) do
      {
        'vin' => vehicle3.vin,
        'year' => vehicle3.year,
        'grade' => vehicle3.grade.to_i,
        'odometer' => vehicle3.odometer,
        'make' => vehicle3.make,
        'model' => vehicle3.model,
        'trim' => vehicle3.trim,
        'bodyStyle' => vehicle3.body_style,
        'fuelType' => vehicle3.fuel_type,
        'hasConditionReport' => vehicle3.has_condition_report,
        'has360' => vehicle3.has_360,
        'consignor' => { 'name' => vehicle3.consignor.name },
        'auction' => { 'name' => vehicle3.company.name }
      }
    end

    it do
      expect(subject.dig('errors', 0, 'message')).to eq(
        'You must filter by at least one, make, body style, consignor, fuel type or has 360'
      )
    end

    context 'when search by make' do
      let(:args_string) { 'make: [Toyota]' }
      let(:vehicle1_atts) { { make: 'Toyota' } }
      let(:vehicle2_atts) { { make: 'Honda' } }
      let(:vehicle3_atts) { { make: 'Toyota' } }

      it do
        expect(subject.dig('data', 'search')).to contain_exactly(
          vehicle1_expected, vehicle3_expected
        )
      end

      context 'and search by model too' do
        let(:args_string) { 'make: [Toyota], model: [Supra]' }
        let(:vehicle1_atts) { { make: 'Toyota', model: 'Supra' } }
        let(:vehicle2_atts) { { make: 'Honda', model: 'Pilot' } }
        let(:vehicle3_atts) { { make: 'Toyota', model: 'Supra' } }

        it do
          expect(subject.dig('data', 'search')).to contain_exactly(
            vehicle1_expected, vehicle3_expected
          )
        end

        context 'and search by trim too' do
          let(:args_string) { 'make: [Toyota], model: [Supra], trim: [SE]' }
          let(:vehicle1_atts) { { make: 'Toyota', model: 'Supra', trim: 'SE' } }
          let(:vehicle2_atts) { { make: 'Honda', model: 'Pilot', trim: 'EX' } }
          let(:vehicle3_atts) { { make: 'Toyota', model: 'Supra', trim: 'Hybrid XLE' } }

          it { expect(subject.dig('data', 'search')).to contain_exactly(vehicle1_expected) }
        end
      end
    end

    context 'when search by body style' do
      let(:args_string) { 'body: [Convertible]' }
      let(:vehicle1_atts) { { body_style: 'Cargo Van' } }
      let(:vehicle2_atts) { { body_style: 'Convertible' } }
      let(:vehicle3_atts) { { body_style: 'Convertible' } }

      it do
        expect(subject.dig('data', 'search')).to contain_exactly(
          vehicle2_expected, vehicle3_expected
        )
      end

      context 'and search by year too' do
        let(:args_string) { 'body: [Convertible], lowYear: 2015' }
        let(:vehicle1_atts) { { body_style: 'Cargo Van', year: 2019 } }
        let(:vehicle2_atts) { { body_style: 'Convertible', year: 2010 } }
        let(:vehicle3_atts) { { body_style: 'Convertible', year: 2015 } }

        it { expect(subject.dig('data', 'search')).to contain_exactly(vehicle3_expected) }
      end
    end

    context 'when search by consignor' do
      let(:args_string) { 'consignor: [Alderson]' }
      let(:consignor1) { FactoryBot.create(:consignor, id: 1, name: 'Alderson') }
      let(:consignor2) { FactoryBot.create(:consignor, id: 2, name: 'Ameren Missouri') }
      let(:vehicle1_atts) { { consignor_id: consignor1.id } }
      let(:vehicle2_atts) { { consignor_id: consignor1.id } }
      let(:vehicle3_atts) { { consignor_id: consignor2.id } }

      it do
        expect(subject.dig('data', 'search')).to contain_exactly(
          vehicle1_expected, vehicle2_expected
        )
      end

      context 'and search by grade too' do
        let(:args_string) { 'consignor: [Alderson], highGrade: 3' }
        let(:vehicle1_atts) { { consignor_id: consignor1.id, grade: 3 } }
        let(:vehicle2_atts) { { consignor_id: consignor1.id, grade: 4 } }
        let(:vehicle3_atts) { { consignor_id: consignor2.id, grade: 5 } }

        it { expect(subject.dig('data', 'search')).to contain_exactly(vehicle1_expected) }
      end
    end

    context 'when search by fuel type' do
      let(:args_string) { 'fuel: [Diesel]' }
      let(:vehicle1_atts) { { fuel_type: 'Diesel' } }
      let(:vehicle2_atts) { { fuel_type: 'Diesel' } }
      let(:vehicle3_atts) { { fuel_type: 'Gasoline Hybrid' } }

      it do
        expect(subject.dig('data', 'search')).to contain_exactly(
          vehicle1_expected, vehicle2_expected
        )
      end

      context 'and search by odometer too' do
        let(:args_string) { 'fuel: [Diesel], lowOdometer: 50000, highOdometer: 70000' }
        let(:vehicle1_atts) { { fuel_type: 'Diesel', odometer: 60_000 } }
        let(:vehicle2_atts) { { fuel_type: 'Diesel', odometer: 90_000 } }
        let(:vehicle3_atts) { { fuel_type: 'Gasoline Hybrid', odometer: 60_000 } }

        it { expect(subject.dig('data', 'search')).to contain_exactly(vehicle1_expected) }
      end
    end

    context 'when search by has 360' do
      let(:args_string) { 'rotation: true' }
      let(:vehicle1_atts) { { has_360: true } }
      let(:vehicle2_atts) { { has_360: true } }
      let(:vehicle3_atts) { { has_360: false } }

      it do
        expect(subject.dig('data', 'search')).to contain_exactly(
          vehicle1_expected, vehicle2_expected
        )
      end

      context 'and search by has_360 too' do
        let(:args_string) { 'rotation: true, hasCr: true' }
        let(:vehicle1_atts) { { has_360: true, has_condition_report: true } }
        let(:vehicle2_atts) { { has_360: true, has_condition_report: false } }
        let(:vehicle3_atts) { { has_360: false, has_condition_report: false } }

        it { expect(subject.dig('data', 'search')).to contain_exactly(vehicle1_expected) }
      end

      context 'and search by auction' do
        let(:args_string) { 'rotation: true, auction: [Northwest]' }
        let(:company1) { FactoryBot.create(:company, id: 1, name: 'Northwest') }
        let(:company2) { FactoryBot.create(:company, id: 2, name: 'Long Beach Auto Auction') }
        let(:vehicle1_atts) { { has_360: true, company_id: company1.id } }
        let(:vehicle2_atts) { { has_360: false, company_id: company1.id } }
        let(:vehicle3_atts) { { has_360: false, company_id: company2.id } }

        it { expect(subject.dig('data', 'search')).to contain_exactly(vehicle1_expected) }
      end
    end
  end
end
