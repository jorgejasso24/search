FactoryBot.define do
  factory :consignor do
    name { Faker::Company.name }
  end
end