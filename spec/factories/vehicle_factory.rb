
FactoryBot.define do
  factory :vehicle, class: Vehicle do
    vin { Faker::Vehicle.vin }
    year { rand(2010..2019) }
    grade { rand(0..5) }
    odometer { rand(10_000...100_000) }
    make { Faker::Vehicle.make }
    model { Faker::Vehicle.model }
    trim { Faker::Vehicle.style }
    body_style { Faker::Vehicle.car_type }
    fuel_type { Faker::Vehicle.fuel_type }
    has_condition_report { false }
    has_360 { false }
    consignor { build(:consignor) }
    company { build(:company) }
  end
end