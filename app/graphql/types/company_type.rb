# frozen_string_literal: true

module Types
  class CompanyType < BaseObject
    field :name, String, null: false
  end
end