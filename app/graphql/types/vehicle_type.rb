# frozen_string_literal: true

module Types
  class VehicleType < BaseObject
    field :vin, String, null: false
    field :year, Integer, null: false
    field :grade, Integer, null: false
    field :odometer, Integer, null: false
    field :make, String, null: false
    field :model, String, null: false
    field :trim, String, null: false
    field :body_style, String, null: false
    field :fuel_type, String, null: false
    field :has_condition_report, Boolean, null: false
    field :has_360, Boolean, null: false
    field :consignor, ConsignorType, null: false
    field :company, CompanyType, null: false
  end
end
