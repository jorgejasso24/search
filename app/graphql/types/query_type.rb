# frozen_string_literal: true

module Types
  class QueryType < Types::BaseObject
    field :years, [Integer], null: false, description: 'Return available years'
    field :grade, [String], null: false, description: 'Return available grades'
    field :odometer, [Integer], null: false, description: 'Return available odometers'
    field :makes, [String], null: false, description: 'Show vehicle makes'
    field :body, [String], null: false, description: 'Show vehicle body styles'
    field :fuel, [String], null: false, description: 'Show vehicle fuel types'
    field :consignor, [String], null: false, description: 'Return all of the consignors'
    field :auction, [String], null: false, description: 'Return all of the auctions'
    field :find_vehicle_by_vin, VehicleType, null: false, description: "Find vehicle by it's VIN" do
      argument :vin, String, required: true
    end
    field :models, [String], null: false, description: 'Show vehicle models by their makes ' do
      argument :make, [String], required: true
    end
    field :trim, [String], null: false, description: 'Show vehicle trims by makes and models' do
      argument :make, [String], required: true
      argument :model, [String], required: true
    end
    field :search, [VehicleType], null: false, description: 'Search vehicles by parameters' do
      argument :low_year, Integer, required: false
      argument :high_year, Integer, required: false
      argument :low_grade, Integer, required: false
      argument :high_grade, Integer, required: false
      argument :low_odometer, Integer, required: false
      argument :high_odometer, Integer, required: false
      argument :make, [String], required: false
      argument :model, [String], required: false
      argument :trim, [String], required: false
      argument :body, [String], required: false
      argument :fuel, [String], required: false
      argument :consignor, [String], required: false
      argument :auction, [String], required: false
      argument :has_cr, Boolean, required: false
      argument :rotation, Boolean, required: false
    end

    def find_vehicle_by_vin(vin:)
      vehicle = Vehicle.find_by(vin: vin)
      return vehicle if vehicle.present?
      raise GraphQL::ExecutionError, "El VIN no existe"
    end

    def makes
      select_field(:make)
    end

    def models(make:)
      condition_field({ make: make }, :model)
    end

    def trim(make:, model:)
      condition_field({ make: make, model: model }, :trim)
    end

    def body
      select_field(:body_style)
    end

    def fuel
      select_field(:fuel_type)
    end

    def consignor
      Consignor.select(:name).pluck(:name).sort
    end

    def auction
      Company.select(:name).pluck(:name).sort
    end

    def years
      select_field(:year)
    end

    def grade
      select_field(:grade)
    end

    def odometer
      select_field(:odometer)
    end

    def search(**args)
      args = default_values(args)
      msg = "You must filter by at least one, make, body style, consignor, fuel type or has 360"
      if [args[:make], args[:body], args[:consignor], args[:fuel], args[:rotation]].compact.blank?
        raise GraphQL::ExecutionError, msg
      end
      hash = assign_hash(args).delete_if { |_key, value| value.blank? }
      Vehicle.joins(:consignor, :company).where(hash)
    end

    private

    def select_field(hash)
      Vehicle.select(hash).distinct.pluck(hash).sort
    end

    def condition_field(hash, field)
      Vehicle.where(hash).distinct.pluck(field).sort
    end

    def assign_hash(args)
      { consignors: { name: args[:consignor] }.compact,
        companies: { name: args[:auction] }.compact,
        vehicles: {
          make: args[:make],
          model: args[:model],
          trim: args[:trim],
          body_style: args[:body],
          fuel_type: args[:fuel],
          year: args[:low_year]..args[:high_year],
          grade: args[:low_grade]..args[:high_grade],
          odometer: args[:low_odometer]..args[:high_odometer],
          has_condition_report: args[:has_cr],
          has_360: args[:rotation]
        }.compact }
    end

    def default_values(args)
      default = {
        :low_year => 0,
        :high_year => 2019,
        :low_grade => 0,
        :high_grade => 5,
        :low_odometer => 0,
        :high_odometer => 100_000
      }
      default.each { |key, value| args[key] = value if args[key].blank? }
      args
    end
  end
end
