# frozen_string_literal: true

module Types
  class ConsignorType < BaseObject
    field :name, String, null: false
  end
end