# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database
# with its default values.
# The data can then be loaded with the rails db:seed command (
# or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

VIN = 20.times.map { Faker::Vehicle.vin }
MAKE = 20.times.map { Faker::Vehicle.make }
MODEL = 20.times.map { Faker::Vehicle.model }
TRIM = 20.times.map { Faker::Vehicle.style }
BODY_STYLE = 10.times.map { Faker::Vehicle.car_type }
FUEL_TYPE = 5.times.map { Faker::Vehicle.fuel_type }
TYPE = ['Dealer Pre-sale', 'Public Pre-sale', 'Factory Pre.sale', 'Buy Now'].freeze
CONSIGNOR = 4.times.map { Faker::Company.name }
AUCTION = 4.times.map { Faker::Company.name }

VEHICLES = [
  { vin: VIN[0], make: MAKE[0], model: MODEL[19], trim: TRIM[10], body_style: BODY_STYLE[9],
    fuel_type: FUEL_TYPE[4], year: 2019, grade: rand(0..5), odometer: 10_000,
    has_condition_report: true, has_360: false },
  { vin: VIN[1], make: MAKE[1], model: MODEL[18], trim: TRIM[11], body_style: BODY_STYLE[8],
    fuel_type: FUEL_TYPE[3], year: 2019, grade: rand(0..5), odometer: 20_000,
    has_condition_report: true, has_360: false },
  { vin: VIN[2], make: MAKE[2], model: MODEL[17], trim: TRIM[12], body_style: BODY_STYLE[7],
    fuel_type: FUEL_TYPE[2], year: 2018, grade: rand(0..5), odometer: 30_000,
    has_condition_report: true, has_360: true },
  { vin: VIN[3], make: MAKE[3], model: MODEL[16], trim: TRIM[13], body_style: BODY_STYLE[6],
    fuel_type: FUEL_TYPE[1], year: 2018, grade: rand(0..5), odometer: 40_000,
    has_condition_report: true, has_360: false },
  { vin: VIN[4], make: MAKE[4], model: MODEL[15], trim: TRIM[14], body_style: BODY_STYLE[5],
    fuel_type: FUEL_TYPE[0], year: 2017, grade: rand(0..5), odometer: 50_000,
    has_condition_report: false, has_360: true },
  { vin: VIN[5], make: MAKE[5], model: MODEL[14], trim: TRIM[15], body_style: BODY_STYLE[4],
    fuel_type: FUEL_TYPE[4], year: 2017, grade: rand(0..5), odometer: 60_000,
    has_condition_report: true, has_360: false },
  { vin: VIN[6], make: MAKE[6], model: MODEL[13], trim: TRIM[16], body_style: BODY_STYLE[3],
    fuel_type: FUEL_TYPE[3], year: 2016, grade: rand(0..5), odometer: 70_000,
    has_condition_report: false, has_360: false },
  { vin: VIN[7], make: MAKE[7], model: MODEL[12], trim: TRIM[17], body_style: BODY_STYLE[2],
    fuel_type: FUEL_TYPE[2], year: 2016, grade: rand(0..5), odometer: 80_000,
    has_condition_report: true, has_360: false },
  { vin: VIN[8], make: MAKE[8], model: MODEL[11], trim: TRIM[18], body_style: BODY_STYLE[1],
    fuel_type: FUEL_TYPE[1], year: 2015, grade: rand(0..5), odometer: 90_000,
    has_condition_report: false, has_360: true },
  { vin: VIN[9], make: MAKE[9], model: MODEL[10], trim: TRIM[19], body_style: BODY_STYLE[0],
    fuel_type: FUEL_TYPE[0], year: 2015, grade: rand(0..5), odometer: 10_000,
    has_condition_report: true, has_360: false },
  { vin: VIN[10], make: MAKE[10], model: MODEL[9], trim: TRIM[0], body_style: BODY_STYLE[9],
    fuel_type: FUEL_TYPE[4], year: 2014, grade: rand(0..5), odometer: 20_000,
    has_condition_report: false, has_360: true },
  { vin: VIN[11], make: MAKE[11], model: MODEL[8], trim: TRIM[1], body_style: BODY_STYLE[8],
    fuel_type: FUEL_TYPE[3], year: 2014, grade: rand(0..5), odometer: 30_000,
    has_condition_report: true, has_360: false },
  { vin: VIN[12], make: MAKE[12], model: MODEL[7], trim: TRIM[2], body_style: BODY_STYLE[7],
    fuel_type: FUEL_TYPE[2], year: 2013, grade: rand(0..5), odometer: 40_000,
    has_condition_report: false, has_360: true },
  { vin: VIN[13], make: MAKE[13], model: MODEL[6], trim: TRIM[3], body_style: BODY_STYLE[6],
    fuel_type: FUEL_TYPE[1], year: 2013, grade: rand(0..5), odometer: 50_000,
    has_condition_report: true, has_360: false },
  { vin: VIN[14], make: MAKE[14], model: MODEL[5], trim: TRIM[4], body_style: BODY_STYLE[5],
    fuel_type: FUEL_TYPE[0], year: 2012, grade: rand(0..5), odometer: 60_000,
    has_condition_report: false, has_360: true },
  { vin: VIN[15], make: MAKE[15], model: MODEL[4], trim: TRIM[5], body_style: BODY_STYLE[4],
    fuel_type: FUEL_TYPE[4], year: 2012, grade: rand(0..5), odometer: 70_000,
    has_condition_report: true, has_360: false },
  { vin: VIN[16], make: MAKE[16], model: MODEL[3], trim: TRIM[6], body_style: BODY_STYLE[3],
    fuel_type: FUEL_TYPE[3], year: 2011, grade: rand(0..5), odometer: 80_000,
    has_condition_report: true, has_360: true },
  { vin: VIN[17], make: MAKE[17], model: MODEL[2], trim: TRIM[7], body_style: BODY_STYLE[2],
    fuel_type: FUEL_TYPE[2], year: 2011, grade: rand(0..5), odometer: 90_000,
    has_condition_report: true, has_360: false },
  { vin: VIN[18], make: MAKE[18], model: MODEL[1], trim: TRIM[8], body_style: BODY_STYLE[1],
    fuel_type: FUEL_TYPE[1], year: 2010, grade: rand(0..5), odometer: 100_000,
    has_condition_report: false, has_360: false },
  { vin: VIN[19], make: MAKE[19], model: MODEL[0], trim: TRIM[9], body_style: BODY_STYLE[0],
    fuel_type: FUEL_TYPE[0], year: 2010, grade: rand(0..5), odometer: 100_000,
    has_condition_report: true, has_360: false }
].freeze

ASSOCIATIONS = [
  VEHICLES[0, 5],
  VEHICLES[5, 5],
  VEHICLES[9, 5],
  VEHICLES[14, 5]
].freeze

ASSOCIATIONS.each_with_index do |values, index|
  consignor = Consignor.create(name: CONSIGNOR[index])
  auction = Company.create(name: AUCTION[index])
  values.each do |feature|
    vehicle = Vehicle.create(
      vin: feature[:vin],
      year: feature[:year],
      grade: feature[:grade],
      odometer: feature[:odometer],
      make: feature[:make],
      model: feature[:model],
      trim: feature[:trim],
      body_style: feature[:body_style],
      fuel_type: feature[:fuel_type],
      has_condition_report: feature[:has_condition_report],
      has_360: feature[:has_360]
    )
    consignor.vehicles << vehicle
    auction.vehicles << vehicle
  end
end
