class AddConsignorIdToVehicles < ActiveRecord::Migration[5.2]
  def change
    add_column :vehicles, :consignor_id, :integer
  end
end
