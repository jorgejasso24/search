class AddNewFieldsToVehicles < ActiveRecord::Migration[5.2]
  def change
    add_column :vehicles, :year, :integer
    add_column :vehicles, :grade, :string
    add_column :vehicles, :odometer, :integer
    add_column :vehicles, :has_condition_report, :boolean
    add_column :vehicles, :has_360, :boolean
  end
end
