class CreateVehicles < ActiveRecord::Migration[5.2]
  def change
    create_table :vehicles do |t|
      t.string :vin
      t.string :make
      t.string :model
      t.string :trim
      t.string :body_style
      t.string :fuel_type

      t.timestamps
    end
  end
end
